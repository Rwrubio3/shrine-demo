import {
	Count,
	CountSchema,
	Filter,
	repository,
	Where,
} from '@loopback/repository';
import {
	post,
	param,
	get,
	getFilterSchemaFor,
	getWhereSchemaFor,
	patch,
	put,
	del,
	requestBody,
} from '@loopback/rest';
import {User} from '../models';
import {UserRepository} from '../repositories';
import {hash, genSalt} from 'bcryptjs';

export class Credentials {
	username: string;
	password: string;
}

export class UserController {
	salt = '$2a$10$eZVsSqO7XCtEkP0Rwvwa6O';
	constructor(@repository(UserRepository)
	            public userRepository: UserRepository) {
	}

	@post('/users', {
		responses: {
			'200': {
				description: 'User model instance',
				content: {'application/json': {schema: {'x-ts-type': User}}},
			},
		},
	})
	async create(@requestBody() user: User): Promise<User> {
		user.password = await hash(user.password, this.salt);
		const savedUser = await this.userRepository.create(user);
		delete savedUser.password;
		return savedUser;
	}

	@post('/users/login', {
		responses: {
			'200': {
				description: 'User login',
				content: {'application/json': {schema: {'x-ts-type': User}}},
			}
		}
	})
	async login(@requestBody() credentials: Credentials): Promise<{}> {
		const hashString = await hash(credentials.password, this.salt);
		const user = await this.userRepository.find({where: {and: [{username: credentials.username}, {password: hashString}]}});
		return {user: user[0], validated: user.length === 1};
	}

	@get('/users/count', {
		responses: {
			'200': {
				description: 'User model count',
				content: {'application/json': {schema: CountSchema}},
			},
		},
	})
	async count(@param.query.object('where', getWhereSchemaFor(User)) where?: Where,): Promise<Count> {
		return await this.userRepository.count(where);
	}

	@get('/users', {
		responses: {
			'200': {
				description: 'Array of User model instances',
				content: {
					'application/json': {
						schema: {type: 'array', items: {'x-ts-type': User}},
					},
				},
			},
		},
	})
	async find(@param.query.object('filter', getFilterSchemaFor(User)) filter?: Filter,): Promise<User[]> {
		return await this.userRepository.find(filter);
	}

	@patch('/users', {
		responses: {
			'200': {
				description: 'User PATCH success count',
				content: {'application/json': {schema: CountSchema}},
			},
		},
	})
	async updateAll(@requestBody() user: User,
	                @param.query.object('where', getWhereSchemaFor(User)) where?: Where,): Promise<Count> {
		return await this.userRepository.updateAll(user, where);
	}

	@get('/users/{id}', {
		responses: {
			'200': {
				description: 'User model instance',
				content: {'application/json': {schema: {'x-ts-type': User}}},
			},
		},
	})
	async findById(@param.path.number('id') id: number): Promise<User> {
		return await this.userRepository.findById(id);
	}

	@patch('/users/{id}', {
		responses: {
			'204': {
				description: 'User PATCH success',
			},
		},
	})
	async updateById(@param.path.number('id') id: number,
	                 @requestBody() user: User,): Promise<void> {
		await this.userRepository.updateById(id, user);
	}

	@put('/users/{id}', {
		responses: {
			'204': {
				description: 'User PUT success',
			},
		},
	})
	async replaceById(@param.path.number('id') id: number,
	                  @requestBody() user: User,): Promise<void> {
		await this.userRepository.replaceById(id, user);
	}

	@del('/users/{id}', {
		responses: {
			'204': {
				description: 'User DELETE success',
			},
		},
	})
	async deleteById(@param.path.number('id') id: number): Promise<void> {
		await this.userRepository.deleteById(id);
	}
}
