import {Entity, model, property} from '@loopback/repository';

@model()
export class Game extends Entity {
	@property({
		type: 'number',
		id: true,
		generated: true,
	})
	id?: number;

	@property({
		type: 'number',
		required: true,
	})
	userId: number;

	@property({
		type: 'number',
		required: true,
	})
	myScore: number;

	@property({
		type: 'number',
		required: true,
	})
	theirScore: number;


	constructor(data?: Partial<Game>) {
		super(data);
	}
}
