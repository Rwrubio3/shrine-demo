import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SubmitScoreComponent} from './submit-score/submit-score.component';
import {LeaderboardComponent} from './leaderboard/leaderboard.component';
import {LoginComponent} from './login/login.component';
import {SignUpComponent} from './sign-up/sign-up.component';

const routes: Routes = [
	{path: 'score', component: SubmitScoreComponent},
	{path: 'leaderboard', component: LeaderboardComponent},
	{path: '', component: LoginComponent},
	{path: 'signup', component: SignUpComponent},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
