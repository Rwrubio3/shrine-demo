import {Component, OnInit} from '@angular/core';
import {User} from '../shared/models/user';
import {UserService} from '../shared/services/user.service';
import {Router} from '@angular/router';

@Component({
	selector: 'app-sign-up',
	templateUrl: './sign-up.component.html',
	styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

	user = new User();

	constructor(private userService: UserService, private router: Router) {
	}

	ngOnInit() {
	}

	create() {
		this.userService.createUser(this.user).subscribe(result => {
			localStorage.setItem('curUserId', result.id);
			this.router.navigateByUrl('/score');
		});
	}

}
