import {Component, OnInit} from '@angular/core';
import {UserService} from '../shared/services/user.service';
import {Router} from '@angular/router';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	credentials = {
		username: '',
		password: ''
	};
	err: string;

	constructor(private userService: UserService, private router: Router) {
	}

	ngOnInit() {
	}

	login() {
		this.userService.login(this.credentials).subscribe(res => {
			if (res.validated) {
				this.err = '';
				localStorage.setItem('curUserId', res.user.id);
				this.router.navigateByUrl('/leaderboard');
			} else {
				this.err = 'Incorrect username or password';
			}
		});
	}
}
