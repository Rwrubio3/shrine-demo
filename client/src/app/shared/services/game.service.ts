import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/internal/operators';

@Injectable({
	providedIn: 'root'
})
export class GameService {

	apiBaseUrl = 'http://localhost:3000/games';

	constructor(private http: HttpClient) {
	}

	getGames(filter?: string) {
		return this.http.get<any>(`${this.apiBaseUrl}${filter ? filter : ''}`)
			.pipe(map(games => {
				return games;
			}));
	}

	submitGameScore(submission: any) {
		return this.http.post<any>(`${this.apiBaseUrl}`, submission)
			.pipe(map(result => {
				return result;
			}));
	}

	getLeaderboard() {
		return this.getGames()
			.pipe(map(games => {
				// Find all the distinct players and then calculate their stats
				const leaderboard = [];
				const distinctIds = new Set(games.map(g => g.userId));
				distinctIds.forEach(di => {
					const wins = games.filter(f => f.userId === di && f.myScore > f.theirScore).length;
					const losses = games.filter(f => f.userId === di && f.myScore < f.theirScore).length;
					const ppg = games.filter(f => f.userId === di).reduce((arr, game) => arr + game.myScore, 0) / (wins + losses);
					const papg = games.filter(f => f.userId === di).reduce((arr, game) => arr + game.theirScore, 0) / (wins + losses);

					leaderboard.push({
						userId: di,
						wins,
						losses,
						ppg,
						papg
					});
				});
				leaderboard.sort((a, b) => a.wins / a.losses < b.wins / b.losses
					? - 1 : a.wins / a.losses > b.wins / b.losses ? 1 : 0);
				return leaderboard;
			}));
	}
}
