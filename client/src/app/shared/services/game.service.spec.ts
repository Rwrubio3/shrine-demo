import {TestBed} from '@angular/core/testing';

import {GameService} from './game.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {GameScore} from '../models/game-score';
import {UserService} from './user.service';
import {of} from 'rxjs/index';

describe('GameService', () => {
	let mockGames: GameScore[];

	beforeEach(() => TestBed.configureTestingModule({
		imports: [HttpClientTestingModule],
		providers: [GameService]
	}));

	beforeEach(() => {
		mockGames = [
			{
				id: 1,
				userId: 1,
				myScore: 11,
				theirScore: 9
			} as GameScore, {
				id: 2,
				userId: 1,
				myScore: 9,
				theirScore: 11
			} as GameScore, {
				id: 3,
				userId: 2,
				myScore: 13,
				theirScore: 11
			} as GameScore
		];
	});

	it('should be created', () => {
		const service: GameService = TestBed.get(GameService);
		expect(service).toBeTruthy();
	});

	it('should be getting leaderboard', () => {
		const gameServiceSpy = jasmine.createSpyObj('GameService', ['getGames']);
		const httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
		const gameService: GameService = new GameService(<any> httpClientSpy);
		httpClientSpy.get.and.returnValue(of(mockGames));
		gameServiceSpy.getGames.and.returnValue(of(mockGames));
		const expectedLeaderboard = [{
			userId: 1,
			wins: 1,
			losses: 1,
			ppg: 10,
			papg: 10
		}, {
			userId: 2,
			wins: 1,
			losses: 0,
			ppg: 13,
			papg: 11
		}];

		gameService.getLeaderboard().subscribe(leaderboard => {
			expect(leaderboard).toEqual(expectedLeaderboard);
			expect(httpClientSpy.get.calls.count()).toBe(1);
		});
	});
});
