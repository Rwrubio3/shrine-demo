import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/internal/operators';

@Injectable({
	providedIn: 'root'
})
export class UserService {

	apiBaseUrl = 'http://localhost:3000/users';

	constructor(private http: HttpClient) {
	}

	getUserData(id: number) {
		return this.http.get<any>(`${this.apiBaseUrl}/${id}`)
			.pipe(map(user => {
				return user;
			}));
	}

	getUsers() {
		return this.http.get<any>(`${this.apiBaseUrl}`)
			.pipe(map(users => {
				return users;
			}));
	}

	login(credentials: any) {
		return this.http.post<any>(`${this.apiBaseUrl}/login`, credentials)
			.pipe(map(result => {
				return result;
			}));
	}

	createUser(userDetails: any) {
		return this.http.post<any>(`${this.apiBaseUrl}`, userDetails)
			.pipe(map(user => {
				return user;
			}));
	}
}
