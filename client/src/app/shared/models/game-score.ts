export class GameScore {
	id?: number;
	userId: number;
	myScore: number;
	theirScore: number;
}