import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LeaderboardComponent} from './leaderboard/leaderboard.component';
import {LoginComponent} from './login/login.component';
import {SignUpComponent} from './sign-up/sign-up.component';
import {SubmitScoreComponent} from './submit-score/submit-score.component';
import {GameService} from './shared/services/game.service';
import {UserService} from './shared/services/user.service';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
	declarations: [
		AppComponent,
		LeaderboardComponent,
		LoginComponent,
		SignUpComponent,
		SubmitScoreComponent
	],
	imports: [
		BrowserModule,
		FormsModule,
		AppRoutingModule,
		HttpClientModule,
		NgbModule
	],
	providers: [GameService, UserService, HttpClientModule],
	bootstrap: [AppComponent]
})
export class AppModule {
}
