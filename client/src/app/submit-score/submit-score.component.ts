import {Component, OnInit} from '@angular/core';
import {GameService} from '../shared/services/game.service';
import {GameScore} from '../shared/models/game-score';

@Component({
	selector: 'app-submit-score',
	templateUrl: './submit-score.component.html',
	styleUrls: ['./submit-score.component.css']
})
export class SubmitScoreComponent implements OnInit {

	score = new GameScore();
	savedScore = false;

	constructor(private gameService: GameService) {
	}

	ngOnInit() {
		this.score.userId = +localStorage.getItem('curUserId');
	}

	submitScore() {
		this.gameService.submitGameScore(this.score).subscribe(result => {
			this.score = new GameScore();
			this.savedScore = true;
			setTimeout(() => {
				this.savedScore = false;
			}, 3000);
		});
	}

}
