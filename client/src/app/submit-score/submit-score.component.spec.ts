import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SubmitScoreComponent} from './submit-score.component';
import {FormsModule} from '@angular/forms';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('SubmitScoreComponent', () => {
	let component: SubmitScoreComponent;
	let fixture: ComponentFixture<SubmitScoreComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [FormsModule, HttpClientTestingModule],
			declarations: [SubmitScoreComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(SubmitScoreComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
