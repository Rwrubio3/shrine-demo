import {Component, OnInit} from '@angular/core';
import {GameService} from '../shared/services/game.service';
import {UserService} from '../shared/services/user.service';
import {User} from '../shared/models/user';
import {forkJoin} from 'rxjs/index';

@Component({
	selector: 'app-leaderboard',
	templateUrl: './leaderboard.component.html',
	styleUrls: ['./leaderboard.component.css']
})
export class LeaderboardComponent implements OnInit {

	leaderboard = [];
	users: User[] = [];

	constructor(private gameService: GameService, private userService: UserService) {
	}

	ngOnInit() {
		forkJoin(
			this.gameService.getLeaderboard(),
			this.userService.getUsers()
		).subscribe(([leaderboard, users]) => {
			this.leaderboard = leaderboard;
			this.users = users;
		});
	}

	displayUsername(userId: number) {
		return this.users.find(u => u.id === userId).username;
	}
}
