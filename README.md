Create database in mysql

Open mysql in the command line or your mysql client

`Create database shrinedemo;`

`grant all privileges on shrinedemo.* to 'shrinedemo1'@'localhost' identified by 'shr1n3d3m0';`

Make sure angular-cli and node are install, if not, install them.

`npm install -g @angular/cli`
`npm i -g @loopback/cli`

Run `npm install`
Run `npm start`

Run `mysql -u shrinedemo1 -pshr1n3d3m0 shrinedemo < generate-data.sql`
Open browser and navigate to localhost:4200

To run tests `npm test`
