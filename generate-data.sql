insert into User values (1, 'Jack', '$2a$10$eZVsSqO7XCtEkP0Rwvwa6OmQgI9mWiRm3Wwwm2unJS3sWDrexBc8C');
insert into User values (2, 'Roger', '$2a$10$eZVsSqO7XCtEkP0Rwvwa6OmQgI9mWiRm3Wwwm2unJS3sWDrexBc8C');
insert into User values (3, 'Stephen', '$2a$10$eZVsSqO7XCtEkP0Rwvwa6OmQgI9mWiRm3Wwwm2unJS3sWDrexBc8C');

insert into Game values (1, 11, 5, 1);
insert into Game values (2, 11, 9, 1);
insert into Game values (3, 10, 12, 1);
insert into Game values (4, 2, 11, 2);
insert into Game values (5, 11, 0, 2);
insert into Game values (6, 11, 8, 3);